 <div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          VOTING
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="../assets/img/faces/avatar.jpg" />
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                <?php echo htmlspecialchars($_SESSION["username"]); ?>
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="logout.php">
                    <span class="sidebar-mini">  </span>
                    <span class="sidebar-normal"> Logout </span>
                  </a>
                </li>
               <!-- <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> EP </span>
                    <span class="sidebar-normal"> Edit Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> S </span>
                    <span class="sidebar-normal"> Settings </span>
                  </a>
                </li> -->
              </ul>
            </div>
          </div>
        </div>
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="../examples/index.php">
              <i class="material-icons">dashboard</i>
              <p> Home </p>
            </a>
          </li>
        
        <!--  
          <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#tablesExamples">
              <i class="material-icons">grid_on</i>
              <p> Tables
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="tablesExamples">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/tables/regular.html">
                    <span class="sidebar-mini"> RT </span>
                    <span class="sidebar-normal"> Regular Tables </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/tables/extended.html">
                    <span class="sidebar-mini"> ET </span>
                    <span class="sidebar-normal"> Extended Tables </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../examples/tables/datatables.net.html">
                    <span class="sidebar-mini"> DT </span>
                    <span class="sidebar-normal"> DataTables.net </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        -->
         
           <li class="nav-item active ">
            <a class="nav-link" data-toggle="collapse" href="#componentsExamples" aria-expanded="true">
              <i class="material-icons">apps</i>
              <p> Components
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="componentsExamples">
              <ul class="nav">
                <li class="nav-item active ">
                  <a class="nav-link" data-toggle="collapse" href="#componentsCollapse" aria-expanded="true">
                    <span class="sidebar-mini"> MLT </span>
                    <span class="sidebar-normal"> Multi Level Collapse
                      <b class="caret"></b>
                    </span>
                  </a>
                  <div class="collapse show" id="componentsCollapse">
                    <ul class="nav">
                      <li class="nav-item ">
                        <a class="nav-link" href="#0">
                          <span class="sidebar-mini"> E </span>
                          <span class="sidebar-normal"> Example </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../../examples/components/buttons.html">
                    <span class="sidebar-mini"> B </span>
                    <span class="sidebar-normal"> Buttons </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="../../examples/components/grid.html">
                    <span class="sidebar-mini"> GS </span>
                    <span class="sidebar-normal"> Grid System </span>
                  </a>
                </li>
                <li class="nav-item  ">
                  <a class="nav-link" href="../examples/campaign.php">
                    <i class="material-icons">grid_on</i>
                    <p> Campaign </p>
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <ul class="nav">
                <li class="nav-item  ">
                  <a class="nav-link" href="../examples/campaign.php">
                    <i class="material-icons">grid_on</i>
                    <p> Campaign </p>
                  </a>
                </li>
                <li class="nav-item  ">
                  <a class="nav-link" href="../examples/campaign.php">
                    <i class="material-icons">grid_on</i>
                    <p> Campaign </p>
                  </a>
                </li>
                <li class="nav-item  ">
                  <a class="nav-link" href="../examples/campaign.php">
                    <i class="material-icons">grid_on</i>
                    <p> Campaign </p>
                  </a>
                </li>
              
              </ul>

          
        </ul>
      </div>
    </div>